package TCP;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;


public class Server {
	
	static String Filipe_information = "Gosta de python mas gosta mesmo, muito, muito mesmo!!!!"; 
	static String Renan_information = "Mestre em todo tipo de filme de todods os tempos";
	static String Giordana_information = "Melhor desenhista deste lado da galaxia";
	static String Valor_incorreto = "essa Informa��o n�o � valida!!!!!\nreinicie a conex�o!!";
	
	
	public static void main(String[] args) {
		int porta = 1;
		try {
			ServerSocket servidor = new ServerSocket(porta);
			System.out.println("Porta: "+porta+"\nserver is working.....");
		
			Socket cliente = servidor.accept();
			System.out.println("cliente conectado: "+cliente.getInetAddress().getHostAddress());
			
			ObjectOutputStream saida = new ObjectOutputStream(cliente.getOutputStream());
			
			String[] opcoes = {"filipe","Renan","Giordana"};
			saida.flush();// limpa o buffer de saida.
			saida.writeObject(opcoes);// envia um objeto que sera tratado no cliente.
			// envia o dados de escolha e espera resposta do cliente
			
			Scanner resposta = new Scanner(cliente.getInputStream());
			String resposta_cliente = resposta.next();
			
			String envio_final = null;
			switch (resposta_cliente) {
			case "Filipe":
				envio_final = Filipe_information;
				break;
			case "Renan":
				envio_final = Renan_information;
				break;
			case "Giordanna":
				envio_final = Giordana_information;
				break;
			default:
				envio_final = Valor_incorreto;
				break;
			}
			
			saida.flush();
			saida.writeObject(envio_final);
			
			saida.close();
			resposta.close();
			servidor.close();
			cliente.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Outra aplica��o pode estar usando esta porta: "+porta);
		}
				
		
	}

}
