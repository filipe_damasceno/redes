package TCP;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.swing.JOptionPane;

public class Client {
	
	public static void main(String[] args) {
		String host = "enderco ip";
		int porta = 1;
		try {
			Socket cliente = new Socket(host,porta);
			System.out.println("cliente esta conectado.\nServer: "+host+"\nPorta: "+porta);
			
			ObjectInputStream ler = new ObjectInputStream(cliente.getInputStream());
			ObjectOutputStream escrever = new ObjectOutputStream(cliente.getOutputStream());
			
			String[] entrada = (String[])ler.readObject();
			
			// joptionPane
			String valor = (String)JOptionPane.showInputDialog(null, "escolha uma op��o", "Alunos",JOptionPane.INFORMATION_MESSAGE, null, entrada, entrada[0]);
			
			escrever.flush();
			escrever.writeObject(valor);
			
			String menssagem_final = (String) ler.readObject();
			
			JOptionPane.showConfirmDialog(null, menssagem_final);
			
			escrever.close();
			ler.close();
			cliente.close();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}

}
